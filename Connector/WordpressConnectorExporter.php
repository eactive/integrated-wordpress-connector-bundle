<?php

namespace Integrated\Bundle\WordpressConnectorBundle\Connector;

use Doctrine\Common\Persistence\ObjectRepository;
use Integrated\Bundle\ContentBundle\Document\Content\Article;
use Integrated\Common\Channel\ChannelInterface;
use Integrated\Common\Channel\Exporter\ExporterInterface;
use Integrated\Bundle\ChannelBundle\Model\Options;
use Integrated\Bundle\ChannelBundle\Model\Config;
use Seven\RpcBundle\XmlRpc\Client;

/**
 * Class WordpressConnectorExporter
 * @author Vasil Pascal <developer.optimum@gmail.com>
 */
class WordpressConnectorExporter implements ExporterInterface
{
    /**
     * @var
     */
    private $config;

    /**
     * WordpressConnectorExporter constructor.
     *
     * @param ObjectRepository $config
     */
    public function __construct(ObjectRepository $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function export($content, $state, ChannelInterface $channel)
    {
        if (!$content instanceof Article) {
            return;
        }

        $contentId = $content->getId();

        $connectors = $this->config->findAll();
        /** @var Config $connector */
        foreach ($connectors as $connector) {
            if (WordpressConnectorManifest::NAME == $connector->getAdapter()
                && in_array($channel->getId(), $connector->getChannels())
            ) {
                /** @var Options $options */
                $options = $connector->getOptions();
                $data = $options->toArray();

                $client = new Client($data['url']);

                $defParams = array(
                    'blog_id' => '1',
                    'username' => $data['username'],
                    'password' => $data['password'],
                );

                $params = array(
                    'filter' => array(),
                    'fields' => array('custom_fields'),
                );

                $posts = array();
                if ($findPosts = $client->call('wp.getPosts', array_merge($defParams, $params))) {
                    foreach ($findPosts as $findPost) {
                        $customField = array_pop($findPost['custom_fields']);
                        $posts[$customField['value']] = $findPost['post_id'];
                    }
                }

                $method = null;
                if (isset($posts[$contentId])) {
                    $method = 'wp.editPost';
                    $defParams['post_id'] = $posts[$contentId];
                } elseif ($state == 'add') {
                    $method = 'wp.newPost';
                }

                if ($method) {
                    $defParams['content'] = array(
                        'post_type' => 'post',
                        'post_status' => ($state == 'add' ? 'publish' : 'pending'),
                        'post_title' => $content->getTitle(),
                        'post_date' => $content->getPublishTime()->getStartDate(),
                        'post_content' => $content->getContent(),
                        'post_excerpt' => $content->getDescription(),
                        'custom_fields' => array(
                            array(
                                'key' => 'integrated_id',
                                'value' => $content->getId(),
                            ),
                        ),
                    );
                    $client->call($method, $defParams);
                }
            }
        }
    }
}
